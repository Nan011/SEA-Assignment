require_relative "../utilities/utilities"

class User
	attr_accessor :age, :name, :history, :money
	attr_reader :map_showable, :orderable, :history_viewable, :position, :id

	#Class to define derivatived user, it will gives information to the driver and customer
	def initialize(name, age, id)
		@name = name
		@id = id
		@age = age
		@history = ""
		@money = 0
		@map_showable = false
		@orderable = false
		@history_viewable = false
	end

	def get_name()
		return Utilities.simplify_name(@name, 15)
	end

	def set_position(x, y)
		@position = [y, x]
	end
end