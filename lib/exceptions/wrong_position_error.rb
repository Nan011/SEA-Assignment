# a Class to handle if no path solution

class WrongPositionError < NoMethodError
	def initialize(message)
		super(message)
	end
end