# a Class to handle if there is no path solution in specific location

class NoPathSolutionError < NoMethodError
	def initialize(message)
		super(message)
	end
end